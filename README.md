# Streamdeck Clock Image Plugin
### By ThatTom - v1.0

Hey,

This is my first attempt at a plugin for the Stream Deck using the JSON SDK, all the source code is open source to help you get your plugin of the ground and I have added some documentation to the code and here so take a look if you need any help getting started.

![Plugin Screenshot][logo]

[logo]: img/screenshot1.png "Plugin Screenshot"

### Current Features

*  1 Second timeout on the clock (Plan to add configuration in later versions)

